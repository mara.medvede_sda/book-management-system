package service;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.Book;
import lv.sda.bms.model.Review;
import lv.sda.bms.model.db.AuthorRepo;
import lv.sda.bms.model.db.ReviewRepo;

public class ReviewService {
    private ReviewRepo reviewRepo;

    public ReviewService(ReviewRepo reviewRepo) {
        this.reviewRepo = reviewRepo;
    }

    public String save(Book book, Integer score, String comment) {
        Review review = new Review();
        review.setBook(book);
        review.setScore(score);
        review.setComment(comment);
        return reviewRepo.save(review) ? "Saved" : "Not Saved";
    }
}
