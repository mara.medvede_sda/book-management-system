package service;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.db.AuthorRepo;

public class AuthorService {
    private AuthorRepo authorRepo;

    public AuthorService(AuthorRepo authorRepo) {
        this.authorRepo = authorRepo;
    }

    public AuthorService() {
    }

    public String save(String firstName, String lastName) {
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        return authorRepo.save(author) ? "Saved" : "Not Saved";
    }



}
