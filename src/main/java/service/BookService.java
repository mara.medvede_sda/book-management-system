package service;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.Book;
import lv.sda.bms.model.db.AuthorRepo;
import lv.sda.bms.model.db.BookRepo;
import lv.sda.bms.model.repository.AuthorRepository;

public class BookService {
    private BookRepo bookRepo;


      public BookService(BookRepo bookRepo) {
        this.bookRepo = bookRepo;

    }
    public BookService() {}



    public String save(String title, Author author, String description) {
        Book book = new Book();
        book.setTitle(title);
        book.setAuthor(author);
        book.setDescription(description);
        return bookRepo.save(book) ? "Saved" : "Not Saved";
    }
}
