package lv.sda.bms.model.repository;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.Book;
import lv.sda.bms.model.Review;
import lv.sda.bms.model.db.AuthorRepo;
import lv.sda.bms.model.db.ReviewRepo;
import lv.sda.bms.model.ui.AuthorView;
import lv.sda.bms.model.ui.ReviewView;
import lv.sda.bms.model.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import service.AuthorService;
import service.ReviewService;

import java.util.List;

public class ReviewRepository {
    private ReviewView reviewView;
    private ReviewService reviewService;

    public ReviewRepository(){
        reviewView = new ReviewView();
        reviewService = new ReviewService(new ReviewRepo());
        reviewView = new ReviewView(reviewService);
        show();
    }

    private void show() {
        reviewView.show();
    }
    public List<Review> getAllReviews() {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String getAllReviews = "SELECT p from Review p";
        Query selectQuery = session.createQuery(getAllReviews);
        List<Review> allReviews = selectQuery.getResultList();
        session.close();
        return allReviews;

    }

    public void createReview(Book book, Integer score, String comment) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        Review review = new Review();
        review.setBook(book);
        review.setScore(score);
        review.setComment(comment);
        session.save(review);
        session.getTransaction().commit();
    }
}
