package lv.sda.bms.model.repository;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.Book;
import lv.sda.bms.model.db.AuthorRepo;
import lv.sda.bms.model.db.BookRepo;
import lv.sda.bms.model.ui.AuthorView;
import lv.sda.bms.model.ui.BookView;
import lv.sda.bms.model.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import service.AuthorService;
import service.BookService;

import java.util.List;

public class BookRepository {
    private BookView bookView;
    private BookService bookService;

    public BookRepository() {
    }

    public BookRepository(BookRepo bookRepo){
        bookView = new BookView();
        bookService = new BookService(bookRepo);
        show();
    }

    private void show() {
        bookView.show();
    }



    public List<Book> getAllBooks() {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String getAllBooks = "SELECT p from Book p";
        Query selectQuery = session.createQuery(getAllBooks);
        List<Book> allBooks = selectQuery.getResultList();
        session.close();
        return allBooks;

    }

    public void createBook(String title, String bookDescription, Author author) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        Book book = new Book();
        book.setTitle(title);
        book.setDescription(bookDescription);
        book.setAuthor(author);
        session.save(book);
        session.getTransaction().commit();
    }

    public void deleteBook(Book book) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(book);
        transaction.commit();
        session.close();
    }

    public void updateBook(Book book) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(book);
        transaction.commit();
        session.close();
    }

    public Book findBookById(Integer id) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Book book = session.find(Book.class,id);
        session.close();
        return book;


    }



    public List<Book> getBookByName(String searchBook) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String query = "from Book p where p.title = :title";
        Query selectQuery = session.createQuery(query);
        selectQuery.setParameter("title", searchBook);
        List<Book> allBooks = selectQuery.getResultList();
        session.close();
        return allBooks;

    }

    public List<Book> getBookByAuthor(String name, String lastName) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String query = "from Book p where p.author.firstName = :name and p.author.lastName = :lastName";
        Query selectQuery = session.createQuery(query);
        selectQuery.setParameter("name", name);
        selectQuery.setParameter("lastName", lastName);
        List<Book> allBooks = selectQuery.getResultList();
        session.close();
        return allBooks;

    }
}
