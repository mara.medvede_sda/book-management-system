package lv.sda.bms.model.repository;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.db.AuthorRepo;
import lv.sda.bms.model.ui.AuthorView;
import lv.sda.bms.model.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import service.AuthorService;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class AuthorRepository {

    private AuthorView authorView;
    private AuthorService authorService;

    public AuthorRepository() {
    }

    public AuthorRepository(AuthorRepo authorRepo){
        authorView = new AuthorView();
        authorService = new AuthorService(authorRepo);
        authorView = new AuthorView(authorService);
        show();
    }

    private void show() {
        authorView.show();
    }

    public List<Author> getAllAuthors() {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        String getAllAuthors = "SELECT p from Author p";
        Query selectQuery = session.createQuery(getAllAuthors);
        List<Author> allAuthors = selectQuery.getResultList();
        session.close();
        return allAuthors;

    }

    public Integer save(Author author) {
        Integer result = null;
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        result = (Integer) session.save(author);
        transaction.commit();
        session.close();
        return result;

    }
    public Author findAuthorById(Integer id) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Author author = session.find(Author.class,id);
        session.close();
        return author;


    }


    public void updateAuthor(Author author) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(author);
        transaction.commit();
        session.close();
    }
    public void createAuthor(String name, String lastName) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        Author author = new Author();
        author.setFirstName(name);
        author.setLastName(lastName);
        session.save(author);
        session.getTransaction().commit();
    }

    public void deleteAuthor(Author author) {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(author);
        transaction.commit();
        session.close();
    }


}
