package lv.sda.bms.model.db;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import static lv.sda.bms.model.utils.HibernateUtils.getSessionFactory;


public class AuthorRepo {
    public boolean save(Author author){
        Integer result = null;
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        result = (Integer) session.save(author);

        transaction.commit();
        session.close();
        return result != null ? true : false;
    }

}
