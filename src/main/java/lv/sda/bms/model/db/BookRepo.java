package lv.sda.bms.model.db;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.Book;
import lv.sda.bms.model.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class BookRepo {
    public boolean save(Book book){
        Integer result = null;
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        result = (Integer) session.save(book);

        transaction.commit();
        session.close();
        return result != null ? true : false;
    }
}
