package lv.sda.bms.model.ui;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.db.BookRepo;
import lv.sda.bms.model.db.ReviewRepo;
import lv.sda.bms.model.repository.AuthorRepository;
import service.AuthorService;
import service.BookService;
import service.ReviewService;

import javax.swing.*;
import java.awt.*;

public class FirstScreen {
    public FirstScreen() {
    }

    public void show() {
        JFrame frame = new JFrame("Mara's Book Management System");
        frame.setPreferredSize(new Dimension(500, 500));


        JLabel whatToDo = new JLabel("Hello! What do you want to do?", SwingConstants.CENTER);
        whatToDo.setSize(200, 200);


        JButton viewDataButton = new JButton("View data");
        viewDataButton.setPreferredSize(new Dimension(200, 100));
        JButton updateDataButton = new JButton("Update existing data");
        updateDataButton.setPreferredSize(new Dimension(200, 100));
        JButton addDataButton = new JButton("Add new data");
        addDataButton.setPreferredSize(new Dimension(200, 100));
        JButton deleteDataButton = new JButton("Delete data");
        deleteDataButton.setPreferredSize(new Dimension(200, 100));


        viewDataButton.addActionListener(actionEvent -> {
            new ViewDataScreen().show();
            frame.dispose();
        });

        addDataButton.addActionListener(actionEvent -> {
            new AddDataScreen().show();
            frame.dispose();
        });


        JPanel p = new JPanel();
        GroupLayout layout = new GroupLayout(p);
        p.setLayout(layout);

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addComponent(whatToDo)
                        .addComponent(viewDataButton)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)

                                .addComponent(addDataButton)
                                .addComponent(updateDataButton)
                                .addComponent(deleteDataButton))
        );

        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(whatToDo)
                                .addComponent(viewDataButton)
                                .addComponent(updateDataButton))
                        .addComponent(addDataButton)
                        .addComponent(deleteDataButton)
        );



        frame.add(p);
        frame.setSize(500,200);

        frame.show();
    }
}
