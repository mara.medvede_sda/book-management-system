package lv.sda.bms.model.ui;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.Book;
import lv.sda.bms.model.repository.BookRepository;

import javax.swing.*;
import java.awt.*;

public class ListScreen {

    public void showBooks(Book[] bookList) {
        JFrame frame = new JFrame();
        JList list = new JList();
        list.setListData(bookList);
        FlowLayout experimentLayout = new FlowLayout();
        JPanel p = new JPanel();
        p.setLayout(experimentLayout);
        p.setSize(1300, 300);
        p.add(list);
        frame.add(p);
        frame.setSize(1300, 300);
        frame.show();
    }
    public void showAuthors(Author[] authorList) {
        JFrame frame = new JFrame();
        JList list = new JList();
        list.setListData(authorList);
        FlowLayout experimentLayout = new FlowLayout();
        JPanel p = new JPanel();
        p.setLayout(experimentLayout);
        p.setSize(1300, 300);
        p.add(list);
        frame.add(p);
        frame.setSize(1300, 300);
        frame.show();
    }
}
