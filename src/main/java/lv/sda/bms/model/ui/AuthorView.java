package lv.sda.bms.model.ui;

import service.AuthorService;

import javax.swing.*;
import java.awt.*;

public class AuthorView {

    private AuthorService authorService;

    public AuthorView(AuthorService authorService) {
        this.authorService = authorService;
    }

    public AuthorView() {}

    public void show() {
        JFrame frame = new JFrame();

        JLabel firstNameLabel = new JLabel("First Name");
        JLabel lastNameLabel = new JLabel("Last Name");
        JLabel saveResultlabel = new JLabel("");

        JTextField firstNameTextField = new JTextField("",20);
        JTextField lastNameTextField = new JTextField("",20);
        firstNameTextField.setSize(100, 30);
        lastNameTextField.setSize(100, 30);

        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(actionEvent -> {
            String firstName = firstNameTextField.getText();
            String lastName = lastNameTextField.getText();

            String result = authorService.save(firstName, lastName);
            saveResultlabel.setText(result);
        });

        FlowLayout experimentLayout = new FlowLayout();
        JPanel p = new JPanel();
        p.setLayout(experimentLayout);
        p.setSize(1300, 300);
        p.add(saveButton);
        p.add(lastNameLabel);

        p.add(lastNameTextField);
        p.add(firstNameLabel);
        p.add(firstNameTextField);

        p.add(saveResultlabel);

        frame.add(p);

        frame.setSize(1300, 300);


        frame.show();
    }

}

