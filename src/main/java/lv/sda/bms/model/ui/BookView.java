package lv.sda.bms.model.ui;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.repository.AuthorRepository;
import service.AuthorService;
import service.BookService;

import javax.swing.*;
import java.awt.*;

public class BookView {


        private BookService bookService;


        public BookView(BookService bookService) {
            this.bookService = bookService;

        }

        public BookView() {}

        public void show() {
            JFrame frame = new JFrame();

            JLabel titleLabel = new JLabel("Book Title");
            JLabel authorLabel = new JLabel("Author");
            JLabel descriptionLabel = new JLabel("Description");
            JLabel saveResultlabel = new JLabel("");

            JTextField titleTextField = new JTextField("",20);
            JTextField authorTextField = new JTextField("",20);
            JTextField descriptionTextField = new JTextField("",20);
            titleTextField.setSize(100, 30);
            authorTextField.setSize(100, 30);
            descriptionTextField.setSize(100, 30);

            JButton saveButton = new JButton("Save");
            saveButton.addActionListener(actionEvent -> {
                String title = titleTextField.getText();
                Author author= new AuthorRepository().findAuthorById(Integer.parseInt(authorTextField.getText()));
                String description = descriptionTextField.getText();

                String result = bookService.save(title, author,description);
                saveResultlabel.setText(result);
            });

            FlowLayout experimentLayout = new FlowLayout();
            JPanel p = new JPanel();
            p.setLayout(experimentLayout);
            p.setSize(1300, 300);
            p.add(saveButton);
            p.add(titleLabel);

            p.add(titleTextField);
            p.add(authorLabel);
            p.add(authorTextField);
            p.add(descriptionLabel);
            p.add(descriptionTextField);

            p.add(saveResultlabel);

            frame.add(p);

            frame.setSize(1300, 300);


            frame.show();
        }
}
