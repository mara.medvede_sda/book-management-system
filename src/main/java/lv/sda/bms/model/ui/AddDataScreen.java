package lv.sda.bms.model.ui;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.Book;
import lv.sda.bms.model.db.AuthorRepo;
import lv.sda.bms.model.db.BookRepo;
import lv.sda.bms.model.repository.AuthorRepository;
import service.AuthorService;
import service.BookService;

import javax.swing.*;
import java.awt.*;

public class AddDataScreen {


    public void show() {
        JFrame frame = new JFrame("Add Data Screen");
        JLabel bookNameLabel = new JLabel("Book Title");
        JLabel authorIdLabel = new JLabel("Author's ID number");
        JLabel bookDescriptionLabel = new JLabel("Book description");
        JLabel authorNameLabel = new JLabel("Name");
        JLabel authorLastNameLabel = new JLabel("Last Name");
        JTextField bookNameTextField = new JTextField("", 20);
        JTextField authorIdTextField = new JTextField("", 20);
        JTextField bookDescriptionTextField = new JTextField("", 20);
        JTextField authorNameTextField = new JTextField("", 20);
        JTextField authorLastNameTextField = new JTextField("", 20);
        JButton addBook = new JButton("Add Book");
        JButton addAuthor = new JButton("Add author");
        JLabel saveResultlabel = new JLabel("");


        addAuthor.addActionListener(actionEvent -> {
            String firstName = authorNameTextField.getText();
            String lastName = authorLastNameTextField.getText();

            String result = new AuthorService(new AuthorRepo()).save(firstName, lastName);
            saveResultlabel.setText(result);
            saveResultlabel.setForeground(Color.green);
            authorNameTextField.setText("");
            authorLastNameTextField.setText("");
        });

        addBook.addActionListener(actionEvent -> {
            String bookName = bookNameTextField.getText();
            Integer authorId = Integer.parseInt(authorIdTextField.getText());
            String description = bookDescriptionTextField.getText();
            Author author = new AuthorRepository().findAuthorById(authorId);

            String result = new BookService(new BookRepo()).save(bookName, author, description);
            saveResultlabel.setText(result);
            saveResultlabel.setForeground(Color.green);
            bookNameTextField.setText("");
            authorIdTextField.setText("");
            bookDescriptionTextField.setText("");
        });


        JPanel p = new JPanel();
        GroupLayout layout = new GroupLayout(p);
        p.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(bookNameLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorNameLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
        )
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(bookNameTextField, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorNameTextField, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        )
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(authorIdLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorLastNameLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        )
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(authorIdTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorLastNameTextField, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        .addComponent(saveResultlabel, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(bookDescriptionLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(addAuthor, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(bookDescriptionTextField, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(addBook, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))


        );


        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(bookNameLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(bookNameTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorIdLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorIdTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(bookDescriptionLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(bookDescriptionTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(addBook, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))

                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(authorNameLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorNameTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorLastNameLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorLastNameTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(addAuthor, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addGap(20)
                                .addComponent(saveResultlabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
        ));

        frame.add(p);
        frame.setSize(1500, 300);
        frame.show();
    }
}
