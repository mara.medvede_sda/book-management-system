package lv.sda.bms.model.ui;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.Book;
import lv.sda.bms.model.db.BookRepo;
import lv.sda.bms.model.repository.AuthorRepository;
import lv.sda.bms.model.repository.BookRepository;
import service.BookService;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ViewDataScreen {


    public void show() {
        JFrame frame = new JFrame("View Data Screen");
        JLabel bookNameLabel = new JLabel("Book Title");
        JLabel authorNameLabel = new JLabel("First Name");
        JLabel authorLastNameLabel = new JLabel("Last Name");
        JLabel noResultsFound = new JLabel("");
        JTextField bookNameTextField = new JTextField("", 20);
        JTextField authorNameTextField = new JTextField("", 20);
        JTextField authorLastNameTextField = new JTextField("", 20);
        JButton findBook = new JButton("Find book");
        JButton findAuthor = new JButton("Find books by author");
        JButton viewAllAuthors = new JButton("View all authors");
        JButton viewAllBooks = new JButton("View all books");

        findBook.addActionListener(actionEvent -> {
            String bookName = bookNameTextField.getText();
            BookRepository bookRepository = new BookRepository();
            List<Book> resultList = bookRepository.getBookByName(bookName);

            //Sis nestraadaaja
            // Book[] resultObject = (Book[])resultList.toArray();
            Book[] resultObject = resultList.toArray(new Book[resultList.size()]);
            if (resultObject.length > 0) {
                new ListScreen().showBooks(resultObject);
                bookNameTextField.setText("");
            } else noResultsFound.setText("There are no Books with such title");
            noResultsFound.setForeground(Color.red);
        });

        findAuthor.addActionListener(actionEvent -> {
            String name = authorNameTextField.getText();
            String lastName = authorLastNameTextField.getText();
            BookRepository bookRepository = new BookRepository();
            List<Book> resultList = bookRepository.getBookByAuthor(name, lastName);
            Book[] resultObject = resultList.toArray(new Book[resultList.size()]);
            if (resultObject.length > 0) {
                new ListScreen().showBooks(resultObject);
                authorNameTextField.setText("");
                authorLastNameTextField.setText("");
            } else noResultsFound.setText("There are no Books by such author");
            noResultsFound.setForeground(Color.red);
        });

        viewAllBooks.addActionListener(actionEvent -> {
            BookRepository bookRepository = new BookRepository();
            List<Book> resultList = bookRepository.getAllBooks();
            Book[] resultObject = resultList.toArray(new Book[resultList.size()]);
            new ListScreen().showBooks(resultObject);
        });

        viewAllAuthors.addActionListener(actionEvent -> {
            AuthorRepository authorRepository = new AuthorRepository();
            List<Author> resultList = authorRepository.getAllAuthors();
            Author[] resultObject = resultList.toArray(new Author[resultList.size()]);
            new ListScreen().showAuthors(resultObject);
        });


        JPanel p = new JPanel();
        GroupLayout layout = new GroupLayout(p);
        p.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(bookNameLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorNameLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        )
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(bookNameTextField, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorNameTextField, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        )
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(findBook, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorLastNameLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                        )
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(viewAllBooks, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorLastNameTextField, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(findAuthor, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)

                                .addComponent(viewAllAuthors))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        )
        );


        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(bookNameLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(bookNameTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(findBook, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(viewAllBooks, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)

                                .addComponent(authorNameLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorNameTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorLastNameLabel, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(authorLastNameTextField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(findAuthor, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                .addComponent(viewAllAuthors, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addGap(100))

        );


        frame.add(p);
        frame.setSize(1000, 300);
        frame.show();
    }


}