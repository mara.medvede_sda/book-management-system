package lv.sda.bms.model.ui;

import lv.sda.bms.model.Book;
import lv.sda.bms.model.repository.BookRepository;
import service.AuthorService;
import service.ReviewService;

import javax.swing.*;
import java.awt.*;

public class ReviewView {
    private ReviewService reviewService;

    public ReviewView(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    public ReviewView() {}

    public void show() {
        JFrame frame = new JFrame();

        JLabel bookLabel = new JLabel("Book");
        JLabel scoreLabel = new JLabel("Score");
        JLabel commentLabel = new JLabel("Comment");

        JLabel saveResultlabel = new JLabel("");

        JTextField bookTextField = new JTextField("",20);
        JTextField scoreTextField = new JTextField("",20);
        JTextField commentTextField = new JTextField("",20);
        bookTextField.setSize(100, 30);
        scoreTextField.setSize(20, 30);
        commentTextField.setSize(100, 100);

        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(actionEvent -> {
            Book book = new BookRepository().findBookById(Integer.parseInt(bookTextField.getText()));
            Integer score = Integer.parseInt(scoreTextField.getText());
            String comment = commentTextField.getText();

            String result = reviewService.save(book, score, comment);
            saveResultlabel.setText(result);
        });

        FlowLayout experimentLayout = new FlowLayout();
        JPanel p = new JPanel();
        p.setLayout(experimentLayout);
        p.setSize(1300, 300);

        p.add(bookLabel);

        p.add(bookTextField);
        p.add(scoreLabel);
        p.add(scoreTextField);
        p.add(commentLabel);
        p.add(commentTextField);


        p.add(saveButton);
        p.add(saveResultlabel);

        frame.add(p);

        frame.setSize(1300, 300);


        frame.show();
    }

}
