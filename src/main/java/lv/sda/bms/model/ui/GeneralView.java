package lv.sda.bms.model.ui;

import lv.sda.bms.model.Author;
import lv.sda.bms.model.db.ReviewRepo;
import lv.sda.bms.model.repository.AuthorRepository;
import service.AuthorService;
import service.BookService;
import service.ReviewService;

import javax.swing.*;
import java.awt.*;

public class GeneralView {
    private AuthorService authorService;
    private BookService bookService;

    public GeneralView(AuthorService authorService, BookService bookService) {
        this.authorService = authorService;
        this.bookService = bookService;
    }

    public GeneralView() {}
    public void show() {
        JFrame frame = new JFrame("Mara's Book Management System");

        JLabel firstNameLabel = new JLabel("First Name");
        JLabel lastNameLabel = new JLabel("Last Name");
        JLabel saveResultlabel = new JLabel("");


        JLabel titleLabel = new JLabel("Book title");
        JLabel authorLabel = new JLabel("Author");
        JLabel descriptionLabel = new JLabel("Description");



        JTextField firstNameTextField = new JTextField("", 20);
        JTextField lastNameTextField = new JTextField("", 20);

        JTextField titleTextField = new JTextField("", 20);
        JTextField authorTextField = new JTextField("", 20);
        JTextField descriptionTextField = new JTextField("",20);

        firstNameTextField.setSize(100, 30);
        lastNameTextField.setSize(100, 30);

        titleTextField.setSize(100, 30);
        authorTextField.setSize(100, 30);
        descriptionTextField.setSize(100, 30);

        JButton saveAuthorButton = new JButton("Save Author");
        JButton searchAuthorButton = new JButton("Search Books by Author");

        JButton saveBookButton = new JButton("Save Book");
        JButton searchBookInfoButton = new JButton("Search information about Book");
        JButton addReviewButton = new JButton("Add Review");

        saveAuthorButton.addActionListener(actionEvent -> {
            String firstName = firstNameTextField.getText();
            String lastName = lastNameTextField.getText();

            String result = authorService.save(firstName, lastName);
            saveResultlabel.setText(result);
        });

        saveBookButton.addActionListener(actionEvent -> {
            String title = titleTextField.getText();
            Author author= new AuthorRepository().findAuthorById(Integer.parseInt(authorTextField.getText()));
            String description = descriptionTextField.getText();

            String result = bookService.save(title, author,description);
            saveResultlabel.setText(result);
        });

        addReviewButton.addActionListener(actionEvent -> {
            ReviewView reviewView = new ReviewView(new ReviewService(new ReviewRepo()));
            reviewView.show();
        });



        FlowLayout experimentLayout = new FlowLayout();


        JPanel p = new JPanel();

        p.setLayout(experimentLayout);

        p.setSize(700, 300);

        p.add(firstNameLabel);
        p.add(firstNameTextField);

        p.add(lastNameLabel);
        p.add(lastNameTextField);

        p.add(saveAuthorButton);
        p.add(searchAuthorButton);

        p.add(titleLabel);
        p.add(titleTextField);
        p.add(authorLabel);
        p.add(authorTextField);
        p.add(descriptionLabel);
        p.add(descriptionTextField);
        p.add(saveBookButton);
        p.add(searchBookInfoButton);
        p.add(addReviewButton);






        p.add(saveResultlabel);

        frame.add(p);


        frame.setSize(900, 1500);


        frame.show();
    }
}
