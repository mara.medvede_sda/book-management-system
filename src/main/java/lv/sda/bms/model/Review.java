package lv.sda.bms.model;

import javax.persistence.*;

@Entity
@Table(name="reviews")

public class Review {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="reviewId")
    private Integer reviewId;
    @ManyToOne
    @JoinColumn(name = "bookId")
    private Book book;
    @Column(name="score")
    private Integer score;
    @Column(name="commentText")
    private String comment;

    public Review() {}

    public void setBook(Book book) {
        this.book = book;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Book getBook() {
        return book;
    }

    public Integer getScore() {
        return score;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Review{");
        sb.append("reviewId=").append(reviewId);
        sb.append(", book=").append(book);
        sb.append(", score=").append(score);
        sb.append(", comment='").append(comment).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
